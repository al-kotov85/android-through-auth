package com.overteam.starttelemed

import android.content.ComponentName
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton.setOnClickListener {
            val intent = Intent()
            intent.component = ComponentName("ru.sogaz.tm", "ru.sogaz.telemed.NavigationActivity")
            intent.putExtra("NavigationActivity.SogazLoginRequest", gson.toJson(SogazLoginRequest("CODE_VALUE", "STATE_VALUE")))

            val activityInfo = intent.resolveActivityInfo(packageManager, intent.flags)
            if (activityInfo !== null && activityInfo.exported) {

                startActivity(intent)
            } else {
                startActivity(Intent.parseUri("https://play.google.com/store/apps/details?id=ru.sogaz.tm", 0))
            }
        }
    }
}

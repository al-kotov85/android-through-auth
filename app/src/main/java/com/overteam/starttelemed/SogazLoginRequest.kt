package com.overteam.starttelemed

data class SogazLoginRequest(
    val code: String,
    val state: String
)